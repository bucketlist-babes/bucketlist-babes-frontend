import type { GatsbyConfig } from "gatsby";

const config: GatsbyConfig = {
  siteMetadata: {
    title: `Bucketlist Babes`,
    siteUrl: `https://bucketlist-babes.pages.dev/`
  },
  plugins: [
    "gatsby-plugin-sass",
    {
      resolve: 'gatsby-plugin-google-gtag',
      options: {
        "trackingIds": [
          "GTM-5378XZC"
        ]
      }
    },
    "gatsby-plugin-react-helmet", {
      resolve: 'gatsby-plugin-manifest',
      options: {
        "icon": "src/images/icon.png"
      }
    }
  ]
};

export default config;
